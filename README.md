# Mint Engine #
## Engine 3D/2D Open-source para todos! ##
___

### O que é ? ###
A Mint Engine é uma API que além de ter todas as funções da API LWJGL (Light Weight Java Game Library) ainda tem muitos recursos legais para ajudar os desenvolvedores!

### Legal! E como faço para usar? ###
Simples! Na aba de downloads iremos lançar versões de snapshot e estáveis! Basta baixar a de sua preferencia! Porém, infelizmente no momento o projeto está bem inicial, então como unica opção é baixar versões instáveis compiladas para testes :( Ah, e na Wiki que você encontra aqui mesmo no nosso repositório há alguns tutoriais e alguma documentação!

### Hm, eu gostaria de ajudar como posso ? ###
Bem de várias formas! Divulgar o projeto é uma das melhores formas por que assim você ajuda esse projeto a evoluir e crescer, outra forma é se tornando um dos nossos "Doadores de Código" tem umas informações na Wiki é só dar uma passadinha lá :).

## Finalmente obrigado a todos pelo tempo cedido ;) ##