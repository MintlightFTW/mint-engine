package net.mintdev.mintengine;

import net.mintdev.mintengine.core.IGameLogic;

public abstract class GameEngine implements IGameLogic, IGameLogic.IGameEngine {
    private IGameLogic gameEngine;

    public void createGame(IGameLogic game, String gameName) {
        this.gameEngine = game;

        //TODO > Algumas coisas legais

        //TODO END

        runEngineTasks();
    }

    @Override
    public void preInit() {

    }

    @Override
    public void runEngineTasks() {
        preInit();
        getEngine().run();
    }

    @Override
    public IGameLogic getEngine() {
        return gameEngine;
    }

}
