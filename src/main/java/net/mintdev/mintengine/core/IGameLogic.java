package net.mintdev.mintengine.core;

public interface IGameLogic {
    void run();
    void init();
    void update();
    void render();
    void exit();

    interface IGameEngine {
        void runEngineTasks();
        void preInit();
        IGameLogic getEngine();
    }

}
