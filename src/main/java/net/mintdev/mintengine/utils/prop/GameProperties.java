package net.mintdev.mintengine.utils.prop;

public class GameProperties {
    public static final int GAME_NAME = 0;
    public static final int GAME_WEBSITE = 1;
    public static final int GAME_AUTHOR = 2;
    public static final int GAME_MAJOR_VERSION = 3;
    public static final int GAME_MINOR_VERSION = 4;
    public static final int GAME_IS_SNAP = 5;
    public static final int GAME_HAS_AUTH = 6;
    public static final int GAME_HAS_ID = 7;
    public static final int GAME_BUILD_VERSION = 8;

    //Props do autor
    public static final int AUTHOR_WEBSITE = 9;
    public static final int AUTHOR_EMAIL = 10;
    public static final int AUTHOR_IS_DEV = 11;

    //Variaveis privadas inicializadas com valores padrao
    private String gameName = "Default";
    private String gameWebsite = "Default";
    private String gameAuthor = "Default";
    private String gameBuildVersion = "Default";
    private String authorWebsite = "Default";
    private String authorEmail = "Default";

    private int gameMajorVersion = 0;
    private int gameMinorVersion = 0;

    private boolean gameIsSnap = false;
    private boolean gameHasAuth = false;
    private boolean gameHasId = false;
    private boolean authorIsDev = false;


    public void createProperty(int PROP_NAME, String value) {
        if(PROP_NAME == 0) {
            this.gameName = value;
        } else if (PROP_NAME == 1) {
            this.gameWebsite = value;
        } else if (PROP_NAME == 2) {
            this.gameAuthor = value;
        } else if (PROP_NAME == 8) {
            this.gameBuildVersion = value;
        } else if (PROP_NAME == 9) {
            this.authorWebsite = value;
        } else if (PROP_NAME == 10) {
            this.authorEmail = value;
        }
    }

    public void createProperty(int PROP_NAME, int value) {
        if(PROP_NAME == 3) {
            this.gameMajorVersion = value;
        } else if(PROP_NAME == 4) {
            this.gameMinorVersion = value;
        }
    }

    public void createProperty(int PROP_NAME, boolean value) {
        if(PROP_NAME == 5) {
            this.gameIsSnap = value;
        } else if(PROP_NAME == 6) {
            this.gameHasAuth = value;
        } else if (PROP_NAME == 7) {
            this.gameHasId = value;
        } else if (PROP_NAME == 11) {
            this.authorIsDev = value;
        }
    }

    public String getProperty(int PROP_NAME) {
        Object obj;
        if(PROP_NAME == 0) {
            obj = gameName;
            return obj.toString();
        } else if (PROP_NAME == 1) {
            obj = gameWebsite;
            return obj.toString();
        } else if (PROP_NAME == 2) {
            obj = gameAuthor;
            return obj.toString();
        } else if(PROP_NAME == 3) {
            obj = gameMajorVersion;
            return obj.toString();
        } else if(PROP_NAME == 4) {
            obj = gameMinorVersion;
            return obj.toString();
        } else if(PROP_NAME == 5) {
            obj = gameIsSnap;
            return obj.toString();
        } else if(PROP_NAME == 6) {
            obj = gameHasAuth;
            return obj.toString();
        } else if (PROP_NAME == 7) {
            obj = gameHasId;
            return obj.toString();
        } else if (PROP_NAME == 8) {
            obj = gameBuildVersion;
            return obj.toString();
        } else if (PROP_NAME == 9) {
            obj = authorWebsite;
            return obj.toString();
        } else if (PROP_NAME == 10) {
            obj = authorEmail;
            return obj.toString();
        } else if (PROP_NAME == 11) {
            obj = authorIsDev;
            return obj.toString();
        } else {
            return null;
        }
    }

}
